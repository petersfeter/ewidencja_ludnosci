using ewidencja_ludnosci;
using ewidencja_ludnosci.DTOs;
using ewidencja_ludnosci.Interfaces;
using ewidencja_ludnosci.Models;
using ewidencja_ludnosci.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;

namespace TEST_ewidencja_ludnosci
{
    [TestClass]
    public class CitizenControllerTest
    {
        private List<Citizen> GetTestUsers()
        {
            var jsonString = File.ReadAllText("C:/Users/PeterSfeter/Documents/ewidencja_ludnosci/TEST_ewidencja_ludnosci/sampleData.json");
            var users = JsonConvert.DeserializeObject<List<Citizen>>(jsonString);

            return users;
        }
        [TestMethod]
        public void GetVerifiedUsersTest()
        {
            //Arrange
            var mockCitizenService = new Mock<ICitizenService>();
            mockCitizenService.Setup(x => x.GetCitizensVerified())
                .Returns(GetTestUsers().Where(x=>x.IsVerified).ToList());

            //zamockowane logowanie
            var mockAuthService = new Mock<IAuthManager>();
            mockAuthService.Setup(x => x.IsLogged(Guid.NewGuid(), true))
                .Returns(true);

            //Mock IHttpContextAccessor
            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext();
            var fakeLoginToken = "2b15311e-bbab-49f4-8281-4126f8966316";
            context.Request.Headers["LoginToken"] = fakeLoginToken;
            mockHttpContextAccessor.Setup(x => x.HttpContext).Returns(context);

            var controller = new CitizensController(mockCitizenService.Object,mockAuthService.Object, mockHttpContextAccessor.Object);

            //Act
            var result = controller.GetCitizen();
            //Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestResult));
        }

        [TestMethod]
        public void GetNotVerifiedUsersTest()
        {
            //Arrange
            var mockCitizenService = new Mock<ICitizenService>();
            mockCitizenService.Setup(x => x.GetCitizensNotVerified())
                .Returns(GetTestUsers().Where(x=>x.IsVerified == false).ToList());

            //zamockowane logowanie
            var mockAuthService = new Mock<IAuthManager>();
            mockAuthService.Setup(x => x.IsLogged(Guid.NewGuid(), true))
                .Returns(true);

            //Mock IHttpContextAccessor
            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext();
            var fakeLoginToken = "2b15311e-bbab-49f4-8281-4126f8966316";
            context.Request.Headers["LoginToken"] = fakeLoginToken;
            mockHttpContextAccessor.Setup(x => x.HttpContext).Returns(context);

            var controller = new CitizensController(mockCitizenService.Object, mockAuthService.Object, mockHttpContextAccessor.Object);

            //Act
            var result = controller.GetCitizenNotVerified();
            //15 verified
            //Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void GetUserTest()
        {
            //Arrange
            var userId = Guid.Parse("dca51cec-14ee-45da-ba5a-01e193baa594");
            var mockCitizenService = new Mock<ICitizenService>();
            mockCitizenService.Setup(x => x.GetCitizen(userId))
                .Returns(GetTestUsers().FirstOrDefault(x=>x.Id == userId));
            //id usera znajduj�cego si� w bazie
            //zamockowane logowanie
            var mockAuthService = new Mock<IAuthManager>();
            mockAuthService.Setup(x => x.IsLogged(Guid.NewGuid(), true))
                .Returns(true);

            //Mock IHttpContextAccessor
            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext();
            var fakeLoginToken = "2b15311e-bbab-49f4-8281-4126f8966316";
            context.Request.Headers["LoginToken"] = fakeLoginToken;
            mockHttpContextAccessor.Setup(x => x.HttpContext).Returns(context);

            var controller = new CitizensController(mockCitizenService.Object, mockAuthService.Object, mockHttpContextAccessor.Object);

            //Act
            var result = controller.GetById(userId);
            //15 verified
            //Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void SignCitizenTest()
        {
            //Arrange
            var mockCitizenService = new Mock<ICitizenService>();
            var userToCreate = new Mock<NewCitizenDTO>();

            mockCitizenService.Setup(x => x.SignCitizen(userToCreate.Object));
            //zamockowane logowanie
            var mockAuthService = new Mock<IAuthManager>();
            mockAuthService.Setup(x => x.IsLogged(Guid.NewGuid(), true))
                .Returns(true);

            //Mock IHttpContextAccessor
            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext();
            mockHttpContextAccessor.Setup(x => x.HttpContext).Returns(context);

            var controller = new CitizensController(mockCitizenService.Object, mockAuthService.Object, mockHttpContextAccessor.Object);
            controller.ModelState.AddModelError("PESEL", "Required");
            //Act
            var result = controller.PostCitizen(userToCreate.Object);
            //Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void CreateNewCitizenTest()
        {
            //Arrange
            var mockCitizenService = new Mock<ICitizenService>();
            var userToCreate = new Mock<NewCitizenDTO>();

            mockCitizenService.Setup(x => x.CreateCitizen(userToCreate.Object));
            //zamockowane logowanie
            var mockAuthService = new Mock<IAuthManager>();
            mockAuthService.Setup(x => x.IsLogged(Guid.NewGuid(), true))
                .Returns(true);

            //Mock IHttpContextAccessor
            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext();
            var fakeLoginToken = "2b15311e-bbab-49f4-8281-4126f8966316";
            context.Request.Headers["LoginToken"] = fakeLoginToken;
            mockHttpContextAccessor.Setup(x => x.HttpContext).Returns(context);

            var controller = new CitizensController(mockCitizenService.Object, mockAuthService.Object, mockHttpContextAccessor.Object);
            controller.ModelState.AddModelError("Email", "Required");
            //Act
            var result = controller.PostCitizenAdmin(userToCreate.Object);
            //Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void EditCitizenTest()
        {
            //Arrange
            var mockCitizenService = new Mock<ICitizenService>();
            var userToCreate = new Mock<EditCitizenDTO>();

            mockCitizenService.Setup(x => x.EditCitizen(userToCreate.Object));
            //zamockowane logowanie
            var mockAuthService = new Mock<IAuthManager>();
            mockAuthService.Setup(x => x.IsLogged(Guid.NewGuid(), true))
                .Returns(true);

            //Mock IHttpContextAccessor
            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext();
            var fakeLoginToken = "2b15311e-bbab-49f4-8281-4126f8966316";
            context.Request.Headers["LoginToken"] = fakeLoginToken;
            mockHttpContextAccessor.Setup(x => x.HttpContext).Returns(context);

            var controller = new CitizensController(mockCitizenService.Object, mockAuthService.Object, mockHttpContextAccessor.Object);
            controller.ModelState.AddModelError("Email", "Required");
            //Act
            var result = controller.EditCitizenData(userToCreate.Object);
            //Assert
            Assert.IsInstanceOfType(result, typeof(BadRequestObjectResult));
        }

        [TestMethod]
        public void ConfirmCitizenTest()
        {
            //Arrange
            var mockCitizenService = new Mock<ICitizenService>();
            var userId = Guid.Parse("2b15311e-bbab-49f4-8281-4126f8966316");

            mockCitizenService.Setup(x => x.ConfirmCitizen(userId));
            //zamockowane logowanie
            var mockAuthService = new Mock<IAuthManager>();
            mockAuthService.Setup(x => x.IsLogged(Guid.NewGuid(), true))
                .Returns(true);

            //Mock IHttpContextAccessor
            var mockHttpContextAccessor = new Mock<IHttpContextAccessor>();
            var context = new DefaultHttpContext();
            mockHttpContextAccessor.Setup(x => x.HttpContext).Returns(context);

            var controller = new CitizensController(mockCitizenService.Object, mockAuthService.Object, mockHttpContextAccessor.Object);
            
            //Act
            var result = controller.ConfirmCitizen(userId);
            //Assert
            Assert.IsNotNull(result);
        }
    }
}
