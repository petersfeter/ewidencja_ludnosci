﻿using ewidencja_ludnosci.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ewidencja_ludnosci.Interfaces
{
    public interface IAuthManager
    {
        public Guid AdminLogin(LoginDTO login);
        public bool IsLogged(Guid token, bool v);
        public Guid UserLogin(Guid id);
    }
}
