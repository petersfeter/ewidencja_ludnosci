﻿using ewidencja_ludnosci.DTOs;
using ewidencja_ludnosci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ewidencja_ludnosci.Interfaces
{
    public interface ICitizenService
    {
        public List<Citizen> GetCitizensVerified();
        //NotVerified to zgłoszenia
        public List<Citizen> GetCitizensNotVerified();
        public Citizen GetCitizen(Guid id);
        public void SignCitizen(NewCitizenDTO citizenDTO);
        public void CreateCitizen(NewCitizenDTO citizenDTO);
        public Guid ConfirmCitizen(Guid id);
        public bool EditCitizen(EditCitizenDTO citizenDTO);
        void DeleteCitizen(Guid id);
    }
}
