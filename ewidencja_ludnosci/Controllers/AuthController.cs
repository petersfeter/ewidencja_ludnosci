﻿using ewidencja_ludnosci.DTOs;
using ewidencja_ludnosci.Interfaces;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ewidencja_ludnosci
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly IAuthManager _authManager;
        public AuthController(IAuthManager authManager)
        {
            _authManager = authManager;
        }
        /// <summary>
        /// Logowanie admina
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("Login")]
        public IActionResult LoginAdmin([FromBody] LoginDTO login)
        {
            var token = _authManager.AdminLogin(login);
            if (token != Guid.Empty)
                return Ok(token);
            else return Unauthorized();
        }        
    }
}
