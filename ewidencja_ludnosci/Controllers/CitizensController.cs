﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ewidencja_ludnosci.Data;
using ewidencja_ludnosci.Models;
using ewidencja_ludnosci.Interfaces;
using Microsoft.Extensions.Primitives;
using ewidencja_ludnosci.DTOs;
using Microsoft.AspNetCore.Routing;

namespace ewidencja_ludnosci
{
    [Route("api/[controller]")]
    [ApiController]
    public class CitizensController : ControllerBase
    {
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly IAuthManager _authManager;
        private readonly ICitizenService _citizenService;
        public CitizensController(ICitizenService citizenService, IAuthManager authManager, IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
            _authManager = authManager;
            _citizenService = citizenService;
        }
        /// <summary>
        /// Pobranie listy mieszkańców zweryfikowanych
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<List<Citizen>> GetCitizen()
        {
            if (_httpContextAccessor.HttpContext.Request.Headers.ContainsKey("LoginToken"))
            {
                StringValues values;
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("LoginToken", out values);
                var token = values.FirstOrDefault();
                if (token == "")
                    return Unauthorized();
                try
                {
                    if (_authManager.IsLogged(Guid.Parse(token), true))
                    {
                        var result = _citizenService.GetCitizensVerified();
                        return Ok(result);
                    }
                    return Unauthorized();
                }
                catch (Exception e)
                {
                    return Unauthorized();
                }
            }
            else return Unauthorized();
        }
        /// <summary>
        /// Pobranie listy zgłoszeń - mieszkańcy niezweryfikowani 
        /// </summary>
        /// <returns></returns>
        [HttpGet("NotVerified")]
        public ActionResult<List<Citizen>> GetCitizenNotVerified()
        {
            if (_httpContextAccessor.HttpContext.Request.Headers.ContainsKey("LoginToken"))
            {
                StringValues values;
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("LoginToken", out values);
                var token = values.FirstOrDefault();
                if (token == "")
                    return Unauthorized();
                try
                {
                    if (_authManager.IsLogged(Guid.Parse(token), true))
                    {
                        var result = _citizenService.GetCitizensNotVerified();
                        return Ok(result);
                    }
                    return Unauthorized();
                }
                catch (Exception e)
                {
                    return Unauthorized();
                }
            }
            else return Unauthorized();
        }
        /// <summary>
        /// Pobranie zgłoszenia/wpisu mieszkańca
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Citizens/5
        [HttpGet("{id}")]
        public ActionResult<Citizen> GetById(Guid id)
        {
            if (_httpContextAccessor.HttpContext.Request.Headers.ContainsKey("LoginToken"))
            {
                StringValues values;
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("LoginToken", out values);
                var token = values.FirstOrDefault();
                if (token == "")
                    return Unauthorized();
                try
                {
                    if (_authManager.IsLogged(Guid.Parse(token), false))
                    {
                        Citizen citizen = _citizenService.GetCitizen(id);

                        if (citizen == null)
                        {
                            return NotFound();
                        }

                        return Ok(citizen);
                    }
                    return Unauthorized();
                }
                catch (Exception e)
                {
                    return Unauthorized();
                }
            }
            else return Unauthorized();
        }
        /// <summary>
        /// Tworzenie nowego wpisu - zgłoszenie mieszkańca
        /// </summary>
        /// <param name="citizenDTO"></param>
        /// <returns></returns>
        [HttpPost("SignNewCitizen")]
        public ActionResult PostCitizen([FromForm] NewCitizenDTO citizenDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            _citizenService.SignCitizen(citizenDTO);
            return Ok();
        }
        /// <summary>
        /// Tworzenie nowego potwierdzonego wpisu przez administratora
        /// </summary>
        /// <param name="citizenDTO"></param>
        /// <returns></returns>
        [HttpPost("CreateNewCitizen")]
        public ActionResult PostCitizenAdmin([FromForm] NewCitizenDTO citizenDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (_httpContextAccessor.HttpContext.Request.Headers.ContainsKey("LoginToken"))
            {
                StringValues values;
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("LoginToken", out values);
                var token = values.FirstOrDefault();
                if (token == "")
                    return Unauthorized();
                try
                {
                    if (_authManager.IsLogged(Guid.Parse(token), true))
                    {
                        _citizenService.CreateCitizen(citizenDTO);
                        return Ok();
                    }
                    return Unauthorized();
                }
                catch (Exception e)
                {
                    return Unauthorized();
                }
            }
            else return Unauthorized();
        }
        /// <summary>
        /// Edycja wpisu
        /// </summary>
        /// <param name="citizenDTO"></param>
        /// <returns></returns>
        [HttpPost("Edit")]
        public ActionResult EditCitizenData([FromForm] EditCitizenDTO citizenDTO)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            if (_httpContextAccessor.HttpContext.Request.Headers.ContainsKey("LoginToken"))
            {
                StringValues values;
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("LoginToken", out values);
                var token = values.FirstOrDefault();
                if (token == "")
                    return Unauthorized();
                try
                {
                    if (_authManager.IsLogged(Guid.Parse(token), false))
                    {
                        bool success = _citizenService.EditCitizen(citizenDTO);
                        return Ok();
                    }
                    return Unauthorized();
                }
                catch (Exception e)
                {
                    return Unauthorized();
                }
            }
            else return Unauthorized();
        }
        /// <summary>
        /// Potwierdzenie zamieszkania - podanie id, logowanie MzID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpPost("ConfirmCitizen")]
        public ActionResult<Guid> ConfirmCitizen([FromBody] Guid id)
        {
            Guid token = _citizenService.ConfirmCitizen(id);
            if (token == Guid.Empty)
                return NotFound();
            return Ok(token);
        }
        /// <summary>
        /// Usuwanie/wymeldowanie dla aktualnie zalogowanego użytkownika
        /// </summary>
        /// <returns></returns>
        [HttpDelete]
        public ActionResult DeleteCitizen()
        {
            if (_httpContextAccessor.HttpContext.Request.Headers.ContainsKey("LoginToken"))
            {
                StringValues values;
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("LoginToken", out values);
                var token = values.FirstOrDefault();
                if (token == "")
                    return Unauthorized();
                try
                {
                    if (_authManager.IsLogged(Guid.Parse(token), false))
                    {
                        _citizenService.DeleteCitizen(Guid.Parse(token));
                        return Ok();
                    }
                    return Unauthorized();
                }
                catch (Exception e)
                {
                    return Unauthorized();
                }
            }
            else return Unauthorized();
        }
        /// <summary>
        /// Usuwanie zgłoszenia/mieszkańca przez administratora
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("Admin/{id}")]
        public ActionResult DeleteCitizenAdmin(Guid id)
        {
            if (_httpContextAccessor.HttpContext.Request.Headers.ContainsKey("LoginToken"))
            {
                StringValues values;
                _httpContextAccessor.HttpContext.Request.Headers.TryGetValue("LoginToken", out values);
                var token = values.FirstOrDefault();
                if (token == "")
                    return Unauthorized();
                try
                {
                    if (_authManager.IsLogged(Guid.Parse(token), true))
                    {
                        _citizenService.DeleteCitizen(id);
                        return Ok();
                    }
                    return Unauthorized();
                }
                catch (Exception e)
                {
                    return Unauthorized();
                }
            }
            else return Unauthorized();
        }
    }
}
