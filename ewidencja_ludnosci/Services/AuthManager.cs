﻿using ewidencja_ludnosci.Data;
using ewidencja_ludnosci.DTOs;
using ewidencja_ludnosci.Interfaces;
using ewidencja_ludnosci.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ewidencja_ludnosci.Services
{
    public class AuthManager : IAuthManager
    {
        private readonly ewidencja_ludnosciContext _context;
        public AuthManager(ewidencja_ludnosciContext context)
        {
            _context = context;
        }
        /// <summary>
        /// Logowanie administratora
        /// </summary>
        /// <param name="login"></param>
        /// <returns></returns>
        public Guid AdminLogin(LoginDTO login)
        {
            var user = _context.Users.FirstOrDefault(x => x.Login == login.Login);
            if (!CheckPassword(login, user))
            {
                return Guid.Empty;
            }
            var auth = new Auth()
            {
                ExpirationDate = DateTime.UtcNow.AddHours(2),
                Token = user.Id,//Guid.NewGuid(),
                IsAdmin = true
            };
            _context.Auths.Add(auth);
            _context.SaveChanges();
            return auth.Id;
        }
        /// <summary>
        /// Sprawdzenie czy podane hasło jest zgodne z tym z bazy
        /// </summary>
        /// <param name="login"></param>
        /// <param name="user"></param>
        /// <returns></returns>
        public bool CheckPassword(LoginDTO login, User user)
        {
            if (user == null)
                return false;

            SHA256 sha256 = SHA256.Create();
            byte[] bytes = sha256.ComputeHash(Encoding.UTF8.GetBytes(login.Password + login.Login + "ec06d57f-51ca-42ba-a952-f9454a4ed569"));//user.Id));

            // Convert byte array to a string   
            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                builder.Append(bytes[i].ToString("x2"));
            }
            if (user.Password != builder.ToString())
                return false;
            else return true;
        }
        /// <summary>
        /// Funkcja weryfikująca czy użytkownik jest zalogowany
        /// </summary>
        /// <param name="token"></param>
        /// <param name="admin"></param>
        /// <returns></returns>
        public bool IsLogged(Guid token, bool admin)
        {
            var auth = _context.Auths.FirstOrDefault(x => x.Id == token);
            if (auth == null)
                return false;
            if (auth.ExpirationDate < DateTime.UtcNow)
                return false;
            if (admin)
            {
                if (auth.IsAdmin)
                    return true;
                else return false;
            }
            else return true;
        }

        public Guid UserLogin(Guid token)
        {
            var citizen = _context.Citizens.FirstOrDefault(x => x.Id == token);
            if (citizen == null)
                return Guid.Empty;

            var authExists = _context.Auths.FirstOrDefault(x => x.Token == token);

            if (authExists != null)
                return authExists.Id;
            else
            {
                var auth = new Auth()
                {
                    ExpirationDate = DateTime.UtcNow.AddHours(2),
                    Token = citizen.Id,
                    IsAdmin = false
                };
                _context.Auths.Add(auth);
                _context.SaveChanges();
                return auth.Id;
            }
        }
    }
}
