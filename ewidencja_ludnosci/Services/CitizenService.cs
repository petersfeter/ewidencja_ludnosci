﻿using ewidencja_ludnosci.Data;
using ewidencja_ludnosci.DTOs;
using ewidencja_ludnosci.Interfaces;
using ewidencja_ludnosci.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace ewidencja_ludnosci.Services
{
    public class CitizenService : ICitizenService
    {
        private readonly IAuthManager _authManager;
        private readonly ewidencja_ludnosciContext _context;
        public CitizenService(ewidencja_ludnosciContext context, IAuthManager authManager)
        {
            _authManager = authManager;
            _context = context;
        }

        public List<Citizen> GetCitizensVerified()
        {
            var result = _context.Citizens.Where(x => x.IsVerified == true).ToList();
            return result;
        }
        public List<Citizen> GetCitizensNotVerified()
        {
            var result = _context.Citizens.Where(x => x.IsVerified == false).ToList();
            return result;
        }

        public Citizen GetCitizen(Guid id)
        {
            var citizen = _context.Citizens.FirstOrDefault(x => x.Id == id);
            return citizen;
        }

        public void SignCitizen(NewCitizenDTO citizenDTO)
        {
            var citizen = new Citizen()
            {
                Age = citizenDTO.Age,
                ApartmentNo = citizenDTO.ApartmentNo,
                CheckedIn = false,
                Email = citizenDTO.Email,
                IsVerified = false,
                Name = citizenDTO.Name,
                PESEL = citizenDTO.PESEL,
                PhoneNumber = citizenDTO.PESEL,
                Postal = citizenDTO.Postal,
                Sex = Enum.Parse<Enums.Sex>(citizenDTO.Sex, true),
                Street = citizenDTO.Street,
                StreetNo = citizenDTO.StreetNo,
                Surname = citizenDTO.Surname
            };
            if (citizenDTO.File == null)
            {
                citizen.FileData = null;
            }
            else
            {
                using (var memoryStream = new MemoryStream())
                {
                    citizenDTO.File.CopyTo(memoryStream);
                    citizen.FileData = memoryStream.ToArray();
                }
            }
            _context.Citizens.Add(citizen);
            _context.SaveChanges();
        }

        public void CreateCitizen(NewCitizenDTO citizenDTO)
        {
            var citizen = new Citizen()
            {
                Age = citizenDTO.Age,
                ApartmentNo = citizenDTO.ApartmentNo,
                CheckedIn = true,
                Email = citizenDTO.Email,
                IsVerified = true,
                Name = citizenDTO.Name,
                PESEL = citizenDTO.PESEL,
                PhoneNumber = citizenDTO.PESEL,
                Postal = citizenDTO.Postal,
                Sex = Enum.Parse<Enums.Sex>(citizenDTO.Sex),
                Street = citizenDTO.Street,
                StreetNo = citizenDTO.StreetNo,
                Surname = citizenDTO.Surname
            };
            _context.Citizens.Add(citizen);
            _context.SaveChanges();
        }

        public Guid ConfirmCitizen(Guid id)
        {
            var citizen = _context.Citizens.FirstOrDefault(x => x.Id == id);
            if (citizen == null)
                return Guid.Empty;
            citizen.CheckedIn = true;
            citizen.IsVerified = true;
            _context.Citizens.Update(citizen);
            var token = _authManager.UserLogin(id);
            _context.SaveChanges();
            return token;
        }

        public bool EditCitizen(EditCitizenDTO citizenDTO)
        {
            var citizenFromBase = _context.Citizens.FirstOrDefault(x => x.Id == citizenDTO.Id);
            if (citizenFromBase == null)
                return false;

            citizenFromBase.Age = citizenDTO.Age;
            citizenFromBase.ApartmentNo = citizenDTO.ApartmentNo;
            citizenFromBase.CheckedIn = false;
            citizenFromBase.Email = citizenDTO.Email;
            citizenFromBase.IsVerified = false;
            citizenFromBase.Name = citizenDTO.Name;
            citizenFromBase.PESEL = citizenDTO.PESEL;
            citizenFromBase.PhoneNumber = citizenDTO.PESEL;
            citizenFromBase.Postal = citizenDTO.Postal;
            citizenFromBase.Sex = Enum.Parse<Enums.Sex>(citizenDTO.Sex);
            citizenFromBase.Street = citizenDTO.Street;
            citizenFromBase.StreetNo = citizenDTO.StreetNo;
            citizenFromBase.Surname = citizenDTO.Surname;

            if (citizenDTO.File == null)
            {
                citizenFromBase.FileData = null;
            }
            else
            {
                using (var memoryStream = new MemoryStream())
                {
                    citizenDTO.File.CopyTo(memoryStream);
                    citizenFromBase.FileData = memoryStream.ToArray();
                }
            }
            _context.Citizens.Update(citizenFromBase);
            _context.SaveChanges();
            return true;
        }

        public void DeleteCitizen(Guid id)
        {
            var auth = _context.Auths.FirstOrDefault(x => x.Id == id);
            if (auth != null)
            {
                var fromBase = _context.Citizens.Find(auth.Token);
                if (fromBase == null)
                    return;
                else
                {
                    _context.Citizens.Remove(fromBase);
                    _context.SaveChanges();
                }
            }
            else
            {
                var fromBase = _context.Citizens.Find(id);
                if (fromBase == null)
                    return;
                else
                {
                    _context.Citizens.Remove(fromBase);
                    _context.SaveChanges();
                }
            }
        }
    }
}
