﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ewidencja_ludnosci.Enums
{
    public enum Sex
    {
        Male = 0,
        Female = 1
    }
}
