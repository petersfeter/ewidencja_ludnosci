﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ewidencja_ludnosci.Models
{
    public class Auth
    {
        public Guid Id { get; set; }
        public Guid Token { get; set; }
        public DateTime ExpirationDate { get; set; }
        public bool IsAdmin { get; set; }
        //public Guid UserId { get; set; }
    }
}
