﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ewidencja_ludnosci.Models;

namespace ewidencja_ludnosci.Data
{
    public class ewidencja_ludnosciContext : DbContext
    {
        public ewidencja_ludnosciContext (DbContextOptions<ewidencja_ludnosciContext> options)
            : base(options)
        {
        }

        public DbSet<Citizen> Citizens { get; set; }
        public DbSet<User> Users { get; set; }
        public DbSet<Auth> Auths { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Citizen>().Property(p => p.Sex).HasConversion<int>();
        }
    }
}
