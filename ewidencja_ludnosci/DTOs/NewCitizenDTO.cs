﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ewidencja_ludnosci.DTOs
{
    public class NewCitizenDTO
    {
        [Required]
        public string Name { get; set; }
        [Required]
        public string Surname { get; set; }
        [Required]
        public int Age { get; set; }
        [Required]
        public string Sex { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public string ApartmentNo { get; set; }
        [Required]
        public string StreetNo { get; set; }
        [Required]
        public string Postal { get; set; }
        [Required]
        public string PhoneNumber { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string PESEL { get; set; }
        public IFormFile File { get; set; }
    }
}
